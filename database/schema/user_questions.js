/**
 * Users Question Schema
 */
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let userQuestionSchema = new Schema({
    question: String,
    option1: String,
    option2: String,
    email: String,
    topic: String,
    status: String
});

module.exports = userQuestionSchema;