const express = require('express');

const app = express();
const bodyParser = require('body-parser');
const path = require('path');
// Check node_env, if not set default to development
const env = ("development");
const config = require('./config')[env];
/**
 * project files
 */
const db = require('./config/database');
const model = require('./models');
const userSeeder = require('./database/seeder/user');


// Configuration, defaults to jade as the view engine
app.engine('ejs', require('express-ejs-extend'));
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/assets', express.static(__dirname + '/public'));
app.set('env', 'dev');

/*
 * This section is for environment specific configuration
 */
// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    if (err) {
        throw err;
    }
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'dev' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error', { error: res.locals.error });
    process.on('uncaughtException', function(error) {
        console.log(error.stack);
    });
});
app.listen(config.EnvConfig.port, function() {
    console.log("http://localhost:3000");
});


/*
 * Exports the express app for other modules to use
 * all route matches go the routes.js file
 */
module.exports = app;

const router = require('./router');