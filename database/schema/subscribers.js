/**
 * Users Schema
 */
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let subscriberSchema = new Schema({
    email: String,
});

module.exports = subscriberSchema;