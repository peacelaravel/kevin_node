/**
 * Poll Schema
 */
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let pollSchema = new Schema({
    question: String,
    background: String,
    status: String
});

module.exports = pollSchema;