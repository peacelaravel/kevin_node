/* require your controllers here */
let app = require('../app');
let loginController = require('../controllers/loginController');
let dashboardController = require('../controllers/dashboardController');
// backend site routes
app.get('/', loginController.index);
app.post('/', loginController.authenticateLogin);

app.get('/dashboard', dashboardController.index);
app.post('/add-category', dashboardController.addCategory);