/**
 * Stats Schema
 */
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let statsSchema = new Schema({
    poll_id: String,
    option: String,
    vote_count: String
});

module.exports = statsSchema;