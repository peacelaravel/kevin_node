/**
 * User Seeder
 */
const User = require('../../models').user;

module.exports = function(app) {
    app.get('/setup', function(req, res) {
        //seed database
        var starterTodos = [{
            name: "Administrator",
            email: 'admin@node.com',
            password: 1234567,
            token: ''
        }];

        User.create(starterTodos, function(err, results) {
            res.send(results);
        });
    })
}