/**
 * Poll Attempt Schema
 */
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let pollAttemptSchema = new Schema({
    subscriber_id: String,
    poll_id: String
});

module.exports = pollAttemptSchema;