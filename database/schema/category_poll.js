/**
 * Category Poll Schema
 * Pivot table
 */
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let categoryPollSchema = new Schema({
    category_id: String,
    poll_id: String
});

module.exports = categoryPollSchema;