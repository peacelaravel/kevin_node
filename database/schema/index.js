/**
 * Export Schema
 */
module.exports = {
    users: require('./users'),
    subscribers: require('./subscribers'),
    categories: require('./categories'),
    polls: require('./polls'),
    stats: require('./stats'),
    user_questions: require('./user_questions'),
    category_poll: require('./category_poll'),
    poll_attempt: require('./poll_attempt'),
}