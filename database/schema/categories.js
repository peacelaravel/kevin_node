/**
 * category schema
 */
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let categorySchema = new Schema({
    name: String,
    icon: String
});

module.exports = categorySchema;